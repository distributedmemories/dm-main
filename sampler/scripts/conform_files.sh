#!/bin/bash

folder="$1"
dry_run="$2"

# Check if folder argument is provided
if [ -z "$folder" ]; then
    echo "Please provide a folder argument."
    exit 1
fi

# Check if folder exists
if [ ! -d "$folder" ]; then
    echo "Folder '$folder' does not exist."
    exit 1
fi

# Set dry run flag
if [ "$dry_run" = "--dry-run" ]; then
    echo "Running in dry run mode."
    dry_run=true
else
    dry_run=false
fi

# Function to rename files
rename_files() {
    local target_folder="$1"

    # Iterate over files in the folder
    for file in "$target_folder"/*; do
        if [[ "$file" == *" "* ]]; then
            # Extract the filename and extension
            filename="${file##*/}"
            extension="${filename##*.}"
            filename="${filename%.*}"

            # Replace spaces with underscores in the filename
            new_filename="${filename// /_}"

            # Construct the new filepath
            new_filepath="$target_folder/$new_filename.$extension"

            if [ "$dry_run" = true ]; then
                echo "Dry run: Would rename '$file' to '$new_filepath'"
            else
                # Rename the file
                mv "$file" "$new_filepath"
                echo "Renamed '$file' to '$new_filepath'"
            fi
        fi

        # Recursive call for subdirectories
        if [ -d "$file" ]; then
            rename_files "$file"
        fi
    done
}

# Start renaming files
rename_files "$folder"
